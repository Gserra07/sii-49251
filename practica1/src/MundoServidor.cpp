// Mundo.cpp: implementation of the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#define MAX_PUNTOS 3
char *proyeccion;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
	{
	      CMundoServidor* p=(CMundoServidor*) d;//mirar aqui
	      p->RecibeComandosJugador();
	}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            //read(tuberiaCli, cad, sizeof(cad));
            SCliente.Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
		}
}
		
CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(pipe);
	/*close(tuberiaCli);
	close(tuberiaServ);
	unlink("/tmp/bot.txt");
	unlink("/tmp/cliente");
	unlink("/tmp/servidor");*/
	
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	//jugador1.Rebota(esfera);
	if(jugador1.Rebota(esfera)){
		if(esfera.radio>0.1)
			esfera.radio-=0.02;
	}
	if(jugador2.Rebota(esfera)){
		if(esfera.radio>0.1)
			esfera.radio-=0.02;
	}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		char cad[100];
		sprintf(cad,"Punto para el Jugador 2!. %d - %d \n",puntos1,puntos2);
		write(pipe,cad,strlen(cad)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		char cad[100];
		sprintf(cad,"Punto para el Jugador 1!. %d - %d \n",puntos1,puntos2);
		write(pipe,cad,strlen(cad)+1);
	}
//Finaliza el programa si uno de los jugadores consigue 3 puntos
	if(puntos1 >= MAX_PUNTOS)
	{
		char cad[100];
		sprintf(cad,"El jugador 1 ha ganado por %d a %d !\n", puntos1, puntos2);
		write(pipe,cad,strlen(cad)+1);
		exit(0);
	}
	if(puntos2 >= MAX_PUNTOS)
	{
		char cad[100];
		sprintf(cad,"El jugador 2 ha ganado por %d a %d !\n", puntos1, puntos2);
		write(pipe,cad,strlen(cad)+1);
		exit(0);
	}
	
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, 	jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//write(tuberiaServ,cad,strlen(cad)+1);
	
	
	//Practica 4
	SCliente.Send(cad,sizeof(cad));
	
	/*
//Control del bot
	dato->esfera=esfera;
	dato->raqueta=jugador2;
	switch (dato->accion)
	{
		case 1:
			OnKeyboardDown('o', 0, 0);break;
		case -1:
			OnKeyboardDown('l', 0, 0);break;
		case 0:
			OnKeyboardDown('t', 0, 0);break;
	}*/
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	//case 't':jugador2.velocidad.y=0;break;

	}
}

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
/*
	//Bot
	int fd;
	
	
	fd=open("/tmp/bot.txt", O_RDWR | O_CREAT | O_TRUNC, 0666);
		 
	write(fd,&datos,sizeof(datos));
 		 
 		 //Se proyecta el fichero 
 	proyeccion=(char*)mmap(NULL,sizeof(*(dato)), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0); 			
		// Se cierra el fichero 
  	close(fd);
 	dato=(DatosMemCompartida*)proyeccion;
	dato->accion=0;
	
	*/
	// Practica 3
	/*
	pipe = open("/tmp/logger", O_WRONLY);
	
	tuberiaServ=open("/tmp/servidor",O_WRONLY);

	tuberiaCli=open("/tmp/cliente", O_RDONLY);*/
	
	pthread_create(&thd1, NULL, hilo_comandos,this);
	
	//Practica 4
	
	char IP[]="127.0.0.1";
	
	SConexion.InitServer(IP,12000);
	
	SCliente=SConexion.Accept();
	SCliente.Receive(NCliente, sizeof(NCliente));
	printf("Cliente: %s\n",NCliente);
}
