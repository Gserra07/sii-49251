#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include "DatosMemCompartida.h"

int main(int argc,char* argv[])
{
	DatosMemCompartida *dato;
	int fd;
	fd=open("/tmp/bot.txt", O_RDWR,0666);
	char *org;
	org=(char*)mmap(NULL, sizeof(dato), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	dato=(DatosMemCompartida *)org;
	while(1)
	{
		
		if((dato->esfera.centro.y)>((dato->raqueta.y1+dato->raqueta.y2)/2))
			dato->accion=1;
		else if((dato->esfera.centro.y)<((dato->raqueta.y1+dato->raqueta.y2)/2))
			dato->accion=-1;
		else
			dato->accion=0;
		usleep(25000);
	}
}
