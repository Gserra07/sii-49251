#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <sys/stat.h>


int main(int argc,char* argv[])
{
	mkfifo("/tmp/logger",0777);
	int pipe=open("/tmp/logger",O_RDONLY);
	
	while(1){		
		char cad[150];
		int resultado = read(pipe,cad,sizeof(cad));	
		if (resultado<=0){
					printf("Error al leer de la tuberia\n");
					break;
		}
		std::cout<<cad<<std::endl;
	}
	
	close(pipe);
	unlink("/tmp/logger");
	return 0;	
}
